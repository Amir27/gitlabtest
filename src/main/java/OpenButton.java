import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class OpenButton
{

    private WebDriver driver;

    @BeforeTest
    public void Setup() {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Praktikant\\eclipse-workspace\\SeleniumTest\\lib\\geckodriver\\geckodriver.exe");
        System.setProperty("webdriver.firefox.bin", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
        driver = new FirefoxDriver();
    }

    @Test
    public void OpenURL() {
        driver.get("https://learn.letskodeit.com/p/practice");
    }

}
